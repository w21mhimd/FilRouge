package ReseauDePetri;

import java.util.ArrayList;
import java.util.List;

/*
 * Cette classe est l'implémentation d'une transition.
 * Chaque transition est associée à des arcs entrants et arcs sortant
 * Une transition peut être tirée à l'aide de la méthode tire()
 */

public class Transition {

	private List<ArcEntrant> arcEntrants;
	private List<ArcSortant> arcSortants;

	// constructeur sans paramètre pour initialiser la liste des arcs entrants/
	// sortants
	public Transition() {

		arcEntrants = new ArrayList<ArcEntrant>();
		arcSortants = new ArrayList<ArcSortant>();
	}

	/*
	 * La méthode parcourt la liste des arcs entrants et vérifie si tous les arcs
	 * sont associés à des places tirables Si la transition est tirable la méthode
	 * déclanche le process du tirage est retourne true sinon elle retourne false
	 */
	public boolean tire() {

		for (ArcEntrant ae : arcEntrants) {
			if (!ae.estTirable()) {
				return false;
			}
		}
		for (ArcEntrant ae : arcEntrants) {
			ae.declanche();
		}
		for (ArcSortant as : arcSortants) {
			as.declanche();
		}
		return true;
	}

	public void ajouterArcEntrant(ArcEntrant ae) {
		arcEntrants.add(ae);
	}

	public void ajouterArcSortant(ArcSortant as) {
		arcSortants.add(as);
	}

	public List<ArcSortant> getArcSortants() {
		return arcSortants;
	}

	public List<ArcEntrant> getArcEntrants() {
		return arcEntrants;
	}

}
