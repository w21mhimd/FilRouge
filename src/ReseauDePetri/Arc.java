package ReseauDePetri;

public abstract class Arc {
	
	private int valeur;
	public void changerValeur(int valeur) {
		this.valeur = valeur;
	}
	public abstract void declanche () ;
	public int getValeur() {
		return valeur;
	}
	
	

}
