package ReseauDePetri;

public class Place {
	
	private int jeton;
	/*public int getJeton() {
		return jeton;
	}*/
	public Place(int jeton) {
		this.jeton=jeton;
	}
	public boolean assezDeJetons(int val) {
		return this.jeton>=val;
	}
	public void enleveJetons(int jetons) {
		this.jeton-=jetons;
	}
	public void ajouteJetons(int jetons) {
		this.jeton+=jetons;
	}
	public void miseAZero() {
		this.jeton=0;
	}
	public boolean estVide() {
		return this.jeton==0;
	}
	
}
