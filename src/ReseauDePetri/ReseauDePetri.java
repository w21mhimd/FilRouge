package ReseauDePetri;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe sert à gérer le réseau de pétri.
 * La classe offre des méthodes pour que l'utilisateur ajoute/ supprime/ modifie  des places/ transitions/ arcs
 * à l'aide d'une interface graphique.
 * Elle lui permet aussi de choisir une transition pour la tirer.
 * 
 */
public class ReseauDePetri {
	private List<Transition> transitions;// Liste des transitions dans le RdP
	private List<Place> places; // Liste des places dans le RdP
	private List<ArcEntrant> arcsEntrants; // Liste des arcs entrant dans le RdP
	private List<ArcSortant> arcsSortants; // Liste des arcs entrant dans le RdP

	// Constructeur sans paramètres
	public ReseauDePetri() {
		transitions = new ArrayList<Transition>();
		places = new ArrayList<Place>();
		arcsEntrants = new ArrayList<ArcEntrant>();
		arcsSortants = new ArrayList<ArcSortant>();

	}
	/** Ajouter une transition dans le réseau de Pétri
	 */ 
	public void ajouteTransition() {
		Transition transition = new Transition();
		transitions.add(transition);

	}
	/** Ajouter une place dans le réseau de Pétri
	 *  @param jetons le nombre de jetons de la place
	 */ 
	public void ajoutePlace(int jetons) {
		Place place = new Place(jetons);
		places.add(place);
	}
	/** Ajouter un arc dans le réseau de Pétri
	 *  @param sens prend true si c'est un arc entrant et false si c'est un arc sortant
	 *  @param place la place associée à l'arc
	 *  @param valeur la valeur de l'arc
	 *  @param transition la transition associée à l'arc
	 */
	public void ajouteArc(boolean sens, Place place, int valeur, Transition transition) {
		if (sens) {
			ArcEntrant ae = new ArcEntrant(valeur, place);
			transition.ajouterArcEntrant(ae);
			arcsEntrants.add(ae);
		} else {
			ArcSortant as = new ArcSortant(valeur, place);
			transition.ajouterArcSortant(as);
			arcsSortants.add(as);
		}

	}

	public boolean supprimeTransition(Transition transition) {
		for (ArcEntrant ae : transition.getArcEntrants()) {
			supprimeArc(ae);
		}
		for (ArcSortant as : transition.getArcSortants()) {
			supprimeArc(as);
		}
		return transitions.remove(transition);
	}

	public boolean supprimePlace(Place place) {
		
		for (ArcEntrant ae : arcsEntrants) {
			if (place == ae.placeDepart) {
				
				arcsEntrants.remove(ae);
			}
		}
		for (ArcSortant as : arcsSortants) {
			if (place == as.placeArrivee ){
				
				arcsSortants.remove(as);
			}
		}
		return places.remove(place);
	}

	public boolean supprimeArc(Arc arc) {
		return arcs.remove(arc);
	}

	// tirer une transition

	public void declanche(Transition transition) {
		if (transitions.contains(transition))
			transition.tire();
	}

	public void changeValeurArc(Arc arc, int valeur) {
		if (arcsEntrants.contains(arc)) {
			arc.changerValeur(valeur);
		}
		if (arcsSortants.contains(arc)) {
			arc.changerValeur(valeur);
		}
	}

	public void changeJetonsPlace(Place place, int valeur) {
		if (places.contains(place)) {
			place.miseAZero();
			place.ajouteJetons(valeur);

		}
	}

	// Getters utilisés pour faire des tests
	/*
	 * public List<Transition> getTransitions() { return transitions; }
	 * 
	 * public List<Place> getPlaces() { return places; }
	 * 
	 * public List<Arc> getArcs() { return arcs; }
	 */
}
