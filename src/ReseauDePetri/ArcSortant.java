package ReseauDePetri;

public class ArcSortant extends Arc {
	
	protected Place placeArrivee;
	public ArcSortant(int valeur, Place p) {
		
		this.changerValeur(valeur);
		this.placeArrivee = p;
	}
	public ArcSortant() {
		
	}
	@Override
	public void declanche() {
		
		placeArrivee.ajouteJetons(getValeur());

	}

	 
	
	

}
