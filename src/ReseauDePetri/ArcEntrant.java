package ReseauDePetri;

public class ArcEntrant extends Arc {
	protected Place placeDepart;
	public ArcEntrant(int valeur,Place p) {
		// TODO Auto-generated constructor stub
		this.changerValeur(valeur);
		this.placeDepart = p;
	}
	@Override
	public void declanche() {
		// TODO Auto-generated method stub
	placeDepart.enleveJetons(getValeur());

	}
	public boolean estTirable() 
	{
		 
		return placeDepart.assezDeJetons(this.getValeur());
	}

}
