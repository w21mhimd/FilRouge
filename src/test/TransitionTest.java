package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ReseauDePetri.ArcEntrant;
import ReseauDePetri.ArcSortant;
import ReseauDePetri.Place;
import ReseauDePetri.Transition;

class TransitionTest {

	@Test
	void testTransition() {
		//fail("Not yet implemented");
	}

	@Test
	void testTire() {
		Transition transition = new Transition();
		Place p1 = new Place(1);
		Place p2 = new Place(3);
		Place p3 = new Place(0);
		Place p4 = new Place(1);
		ArcEntrant ae1 = new ArcEntrant(1,p1);
		ArcEntrant ae2 = new ArcEntrant(2,p2);
		transition.getArcEntrants().add(ae1);transition.getArcEntrants().add(ae2);
		
		ArcSortant as1 = new ArcSortant(3, p3);
		ArcSortant as2 = new ArcSortant(1, p4);
		transition.getArcSortants().add(as1);transition.getArcSortants().add(as2);
		transition.tire();
		assertEquals("Result doit être 0", 0, p1.getJeton());
		assertEquals("Result doit être 1", 1, p2.getJeton());
		assertEquals("Result doit être 3", 3, p3.getJeton());
		assertEquals("Result doit être 2", 2, p4.getJeton());
		
	}

}
