package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ReseauDePetri.Arc;
import ReseauDePetri.ArcSortant;
import ReseauDePetri.Place;
import ReseauDePetri.ReseauDePetri;
import ReseauDePetri.Transition;

class ReseauDePetriTest {

	@Test
	void testAjouteTransition() {
		ReseauDePetri rdp = new ReseauDePetri();
		rdp.ajouteTransition(new Transition());
		assertEquals("Result doit être false", false, rdp.getTransitions().isEmpty());
		
	}

	@Test
	void testAjoutePlace() {
		ReseauDePetri rdp = new ReseauDePetri();
		rdp.ajoutePlace(new Place(4));
		assertEquals("Result doit être false", false, rdp.getPlaces().isEmpty());
	}

	@Test
	void testAjouteArc() {
		ReseauDePetri rdp = new ReseauDePetri();
		rdp.ajouteArc(new ArcSortant());
		assertEquals("Result doit être false", false, rdp.getArcs().isEmpty());
	}

	@Test
	void testSupprimeTransition() {
		ReseauDePetri rdp = new ReseauDePetri();
		Transition t = new Transition();
		rdp.ajouteTransition(t);
		rdp.supprimeTransition(t);
		assertEquals("Result doit être true", true, rdp.getTransitions().isEmpty());
	}

	@Test
	void testSupprimePlace() {
		ReseauDePetri rdp = new ReseauDePetri();
		Place p= new Place(4);
		rdp.ajoutePlace(p);
		rdp.supprimePlace(p);
		assertEquals("Result doit être true", true, rdp.getPlaces().isEmpty());
	}

	@Test
	void testSupprimeArc() {
		ReseauDePetri rdp = new ReseauDePetri();
		Arc a = new ArcSortant();
		rdp.ajouteArc(a);
		rdp.supprimeArc(a);
		assertEquals("Result doit être false", false, !rdp.getArcs().isEmpty());
	}


	@Test
	void testChangeValeurArc() {
		ReseauDePetri rdp = new ReseauDePetri();
		Place p= new Place(4);
		Arc a = new ArcSortant(5,p);
		rdp.ajouteArc(a);
		rdp.changeValeurArc(a,12);
		assertEquals("Result doit être 12", 12, a.getValeur());
		
	}

	@Test
	void testChangeJetonsPlace() {
		ReseauDePetri rdp = new ReseauDePetri();
		Place p= new Place(4);
		rdp.ajoutePlace(p);
		rdp.changeJetonsPlace(p, 8);
		assertEquals("Result doit être 8", 8, p.getJeton());
		
	}

}
