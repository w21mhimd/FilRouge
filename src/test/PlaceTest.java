package test;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import ReseauDePetri.Place;

class PlaceTest {

	/*@Test
	void testGetJeton() {
		fail("Not yet implemented");
	}*/

	@Test
	void testPlace() {
		//("Not yet implemented");
	}

	@Test
	void testAssezDeJetons() {
		Place p1 = new Place(4);
		assertEquals("Result doit être true", true, p1.assezDeJetons(3));
	}

	@Test
	void testEnleveJetons() {
		Place p1 = new Place(4);
		p1.enleveJetons(2);
		assertEquals("Result doit être 2", 2, p1.getJeton());
	}

	@Test
	void testAjouteJetons() {
		Place p1 = new Place(4);
		p1.ajouteJetons(5);
		assertEquals("Result doit être 9", 9, p1.getJeton());
	}

	@Test
	void testMiseAZero() {
		Place p1 = new Place(4);
		p1.miseAZero();
		assertEquals("Result doit être 0", 0, p1.getJeton());
	}

	@Test
	void testEstVide() {
		Place p1 = new Place(0);
		assertEquals("Result doit être true", true, p1.estVide());
	}

}
